round = Round.find(ENV['ROUND'].to_i)

data = Result.timesliced_score_for_round(round, 1.minute)

data.each do |key, scores|
  scores.dup.each_with_index.with_object([0]) do |(cur, i), _prev|
    prev = _prev[0]

    if cur.nil?
      cur = prev
      scores[i] = prev
    end

    _prev[0] = cur
  end
end

csv_path = Rails.root.join('tmp', "round_#{round.id}.csv")

open(csv_path, 'w') do |io|
  teams = data.keys
  teams.delete(name: 'x')

  teams.sort_by! { |k| -(data[k][-1].to_i) }

  teams.prepend(name: 'x')

  title = teams.map { |_| _[:name] }.join(',')
  io.puts(title)
  puts title

  row_count = data.map{ |k,ss| ss.size }.max

  row_count.times do |i|
    io.puts teams.map { |team| data[team][i] }.join(',')
  end
end

