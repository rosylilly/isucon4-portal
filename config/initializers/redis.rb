url = ENV["REDISCLOUD_URL"] || ENV['REDIS_URL']
Redis.current = Redis.new(driver: :hiredis, url: url) if url
