# Be sure to restart your server when you modify this file.

#Rails.application.config.session_store :cookie_store, key: '_isucon4-portal_session'
Rails.application.config.session_store ActionDispatch::Session::CacheStore, key: '_isucon4-portal_session', expire_after: 2.days
