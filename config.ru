# This file is used by Rack-based servers to start the application.

require ::File.expand_path('../config/environment',  __FILE__)
use Rack::Lineprof if ENV['LINEPROF'] == '1'
run Rails.application
