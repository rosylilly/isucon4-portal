class Team < ActiveRecord::Base
  validates :name, presence: true

  has_many :qualifications
  has_many :rounds, through: :qualifications
  has_many :final_reports
  has_many :results
  has_many :submission_logs

  has_many :api_keys

  def participate?(round)
    round.teams.include?(self)
  end
end
