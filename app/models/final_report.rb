class FinalReport < ActiveRecord::Base
  belongs_to :round
  belongs_to :team

  validates :round, presence: true
  validates :team, presence: true
  validates :ami_id, presence: true, format: /\Aami-[a-f0-9A-F]+\Z/

  validate :deadline_exceed

  scope :for, ->(round) { where(round_id: round.id) }

  def deadline_exceed
    errors.add(:base, "AMI submission deadline exceeded.") if self.round.try(&:report_deadline_exceed?)
  end
end
