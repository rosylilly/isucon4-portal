require 'securerandom'

class ApiKey < ActiveRecord::Base
  belongs_to :team
  belongs_to :round

  validates :team, presence: true
  validates :secret, presence: true, uniqueness: true

  validate do
    unless self.team.rounds.include?(self.round) || self.team.admin?
      errors.add(:team, "Team should be participant of round")
    end
  end

  before_validation do
    unless self.secret
      t = Time.now
      str = SecureRandom.hex(20)
      self.secret = "#{team_id}-#{round_id}-#{t.tv_sec.to_s(36)}-#{t.tv_usec.to_s(36)}-#{str}"
    end
  end
end
