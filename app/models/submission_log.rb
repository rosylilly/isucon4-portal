class SubmissionLog < ActiveRecord::Base
  belongs_to :api_key
  belongs_to :team
  belongs_to :result
end
