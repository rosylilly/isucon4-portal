class Result < ActiveRecord::Base
  belongs_to :team
  belongs_to :round
  has_one :submission_log

  validates :team, presence: true
  validates :round, presence: true
  validates :score, presence: true
  validates :successes, presence: true
  validates :fails, presence: true

  validate :available_round, on: :user

  scope :by, ->(team) { where(team_id: team.id) }
  scope :at, ->(round) { where(round_id: round.id) }
  scope :in, ->(range) { where(created_at: range) }
  scope :latest, -> { order(id: :desc).limit(1) }
  scope :highest, -> { order(score: :desc).limit(1).first }

  CACHE_RENEW_TIME = Time.zone.local(2014, 9, 27, 16, 0, 0)

  def self.latest_team_result_at(team, round, time)
  end


  def self.latest_results_at(time, round)
    actual = proc do
      if Time.zone.now < time
        round.teams.map do |team|
          [{id: team.id, name: team.name}, nil]
        end.to_h
      else
        round.teams.map do |team|
          [{id: team.id, name: team.name},
           Result.by(team).at(round).where('created_at <= ?', time).latest.first.try(:score) || nil]
        end.to_h
      end
    end

    if Time.zone.now < time
      actual[]
    else
      key = "results-#{round.id}-#{time.to_s(:db)}"
      if time >= CACHE_RENEW_TIME
        key << "-renew"
      end
      Rails.cache.fetch(key, expires_in: 8.hours, &actual)
    end
  end

  def self.timesliced_score_for_round(round, interval, teams: nil)
    t = round.starts_at

    times = []
    data = {}
    while t <= round.ends_at
      times << t.strftime('%Y-%m-%d %H:%M:%S')

      results = Result.latest_results_at(t, round)
      results.each do |team, score|
        next if teams && !teams.include?(team[:name]) && !teams.include?(team[:id])

        data[team] ||= []
        if block_given? ? yield(round, team, t) : true
          data[team] << score
        else
          data[team] << nil
        end
      end

      t += interval
    end

    data[{name: 'x'}] = times

    data
  end

  private

  def available_round
    errors.add(:round, 'round is not available') unless round.available?
  end
end
