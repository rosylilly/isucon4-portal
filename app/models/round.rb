class Round < ActiveRecord::Base
  has_many :qualifications
  has_many :teams, through: :qualifications

  has_many :api_keys

  validates :starts_at, presence: true
  validates :ends_at, presence: true

  def teams_with_score
    @teams_with_score ||= begin
      team_ids = Result.at(self).group('results.team_id').pluck('results.team_id')
      Team.where(id: team_ids)
    end
  end

  def teams_with_highest_result
    teams.map do |team|
      [team, Result.by(team).at(self).highest]
    end
  end

  def teams_with_latest_result(time = Time.zone.now)
    teams.map do |team|
      [team, Result.by(team).at(self).in(self.starts_at .. time).latest.first]
    end
  end

  def available?(at = Time.zone.now)
    starts_at <= at && ends_at >= at
  end

  def report_deadline_exceed?
    report_deadline_at && report_deadline_at <= Time.zone.now
  end
end
