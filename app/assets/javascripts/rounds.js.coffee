return unless inController('rounds') && (inAction('show') || inAction('kiosk'))

$(() ->
  kioskMode = inAction('kiosk')

  vm = new Vue(
    el: 'body'

    data: {
      graphSpan: 10
      topN: 5
      teams: []
      loadingNow: false
    }

    methods: {
      setGraphSpan: (span) ->
        @graphSpan = span
        reloadScore()

      setTopN: (n) ->
        @topN = n
        reloadScore()

      reloadNow: () ->
        reloadScore()

      fullscreenGraph: () ->
        sg = $('#score-graph')
        screenfull.toggle(sg[0])

        if screenfull.isFullscreen
          sg.addClass('fullscreen')
          chart.resize({ height: sg.height() })
        else
          sg.removeClass('fullscreen')
          chart.resize({ height: 500 })
    }
  )

  document.addEventListener(screenfull.raw.fullscreenchange, () ->
    if screenfull.isFullscreen
      chart.resize({ height: $('#score-graph').height() })
    else
      chart.resize({ height: 500 })
  )

  chart = c3.generate(
    bindto: '#score-chart'
    data: {
      x: 'x'
      xFormat: '%Y-%m-%d %H:%M:%S'
      columns: []
    }
    axis: {
      x: {
        type: 'timeseries',
        tick: {
          format: '%H:%M'
        }
      }
      y: {
        min: 0
      }
    }
    point: {
        show: true
    }
    tooltip: {
      grouped: false
    }
    size: {
      height: if kioskMode then '25%' else 500
    }
    subchart: {
      show: !kioskMode
    }
    legend: {
      show: false
    }
  )

  endpoint = $('.round-description').data('endpoint')
  reloadTimeout = null
  reloadScore = () ->
    vm.loadingNow = true
    clearTimeout(reloadTimeout) if reloadTimeout

    async.parallel([
      ((cb) ->
        $.getJSON(endpoint + "/scores?#{+new Date()}", { span: vm.$data.graphSpan, top: vm.$data.topN }, (data) ->
          chart.load({
            columns: data
          })
          cb()
        )
      ),
      ((cb) ->
        $.getJSON(endpoint + "/self_scores?#{+new Date()}", { span: vm.$data.graphSpan, top: vm.$data.topN }, (data) ->
          chart.load({
            columns: data
          })
          cb()
        )
      ),
      ((cb) ->
        $.getJSON(endpoint + '/standings', (data) ->
          teams = []
          $.each(data, (i, d) ->
            teams.push({
              name: d.team.name,
              score: d.result?.score || 0,
              successes: d.result?.successes || 0,
              fails: d.result?.fails || 0,
              at: d.result?.created_at
            })
          )
          vm.teams = teams
          cb()
        )
      )
    ], () ->
      vm.loadingNow = false
      reloadTimeout = setTimeout(() ->
        reloadScore()
      , 60 * 1000)
    )


  reloadScore()

  endMessage = $('#round-ended')
  if endMessage.length > 0
    endMessage.showOn = moment(endMessage.data('show-on'), 'YYYY-MM-DD HH:mm:ss Z')

    showMessage = () ->
      if endMessage.showOn.isBefore(moment())
        if endMessage.hasClass('modal')
          endMessage.modal('show')
        else
          #endMessage.show()
      else
        setTimeout(showMessage, 10000)

    showMessage()
)
