// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require moment-with-locales
//= require moment-timezone
//= require vue
//= require d3
//= require c3
//= require async
//= require bootstrap/modal
//= require screenfull
//= require_self
//= require_tree .

window.inController = function(controller) {
  controller = controller.replace('/', '-');
  return $('html.' + controller + '-controller').length > 0
};

window.inAction = function(action) {
  return $('html.' + action + '-action').length > 0
};

Vue.filter('relativeTime', function(time) {
  if(time) { time = moment(time) }

  if(time && time.isValid()) {
    return time.fromNow();
  } else {
    return time;
  }
});

Vue.filter('formatTime', function(time) {
  if(time) { time = moment(time) }

  if(time && time.isValid()) {
    return time.format("YYYY/MM/DD h:mm:ss a");
  } else {
    return time;
  }
})
