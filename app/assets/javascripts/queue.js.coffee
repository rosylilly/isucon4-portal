return unless inController('queue')

jQuery ($) ->
  v = new Vue(
    el: '#queue_status'
    data: {
      reloading: false
      retrievedAt: null
      updatedAt: null
      running: []
      pending: []
    }

    methods: {
      reloadNow: () -> do reload
    }
  )

  reload = ->
    return if v.reloading
    v.reloading = true
    $.getJSON("/queue/status?#{Date.now()}", (queue) ->
      v.$data.running = queue.running
      v.$data.pending = queue.pending
      v.$data.retrievedAt = moment().format("YYYY/MM/DD h:mm:ss a")
      v.$data.updatedAt = queue.updated_at
      v.reloading = false
    )

  do reload

  setInterval(reload, 10000)
