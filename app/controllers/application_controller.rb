class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  around_action :set_time_zone

  private

  def set_time_zone
    previous_zone = Time.zone
    Time.zone = 'Asia/Tokyo'
    yield
    Time.zone = previous_zone
  end

  def require_login
    redirect_to login_path unless current_team
  end

  def current_team
    @current_team ||= begin
      if current_api_key
        key = ApiKey.where(secret: current_api_key).first
        if key && key.team
          key.team
        else
          session[:api_key] = nil
        end
      else
        nil
      end
    end
  end
  helper_method :current_team

  def current_api_key
    authtype, authkey = (request.headers['Authorization'] || '').split(/\s+/)
    authkey = nil if authtype != "isucon"

    authkey || session[:api_key]
  end

  def require_admin
    unless current_team && current_team.admin?
      render status: 403, text: "Your team should be an administrator."
    end
  end
end
