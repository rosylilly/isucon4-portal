class ResultsController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :require_api_key

  def record
    result = Result.new(
      team: api_key.team,
      round: api_key.round,
      score: params[:score],
      successes: params[:successes],
      fails: params[:fails]
    )

    context = forced_by_admin? ? :admin : :user
    if result.save(context: context)
      Rails.cache.delete("round-0-standings-#{api_key.round_id}")
      Rails.cache.delete("round-1-standings-#{api_key.round_id}")

      SubmissionLog.create(
        team: api_key.team,
        api_key: api_key,
        result: result,
        ip_address: request.ip,
        message: forced_by_admin? ? 'OK (Forced by admin)' : 'OK',
        meta: params[:metadata].to_json,
      )
    else
      SubmissionLog.create(
        team: api_key.team,
        api_key: api_key,
        ip_address: request.ip,
        message: "not valid: score:#{params[:score]},succ:#{params[:successes]},fail:#{params[:fails]}",
        meta: params[:metadata].to_json,
      )
    end

    render json: { ok: result.persisted? }, status: result.persisted? ? 201 : 403
  end

  private

  def forced_by_admin?
    force_secret = request.headers['X-Force-Admin-Benchmark']
    force_key = ApiKey.where(secret: force_secret).first
    force_key && force_key.team.try(:admin?)
  end

  def api_key
    @api_key ||= ApiKey.where(secret: request.headers['X-API-KEY']).first
  end

  def require_api_key
     unless api_key
       SubmissionLog.create(
         ip_address: request.ip,
         meta: params[:metadata].to_json,
         message: "missing or wrong api key: #{request.headers['X-API-KEY'].inspect}",
       )
       render json: { ok: false }, status: 401
     end
  end
end
