class RoundsController < ApplicationController
  before_action :set_round, only: [:show, :scores, :standings, :edit_report, :submit_report, :edit, :update, :destroy, :self_scores, :kiosk]
  before_action :require_viewer, only: [:show, :scores, :standings]
  before_action :require_participant, only: [:edit_report, :submit_report]
  before_action :require_admin, only: %i(new index create edit update destroy)

  # GET /rounds
  # GET /rounds.json
  def index
    @rounds = Round.all
  end

  # GET /rounds/1
  # GET /rounds/1.json
  def show
  end

  def kiosk
    render :kiosk, layout: nil
  end

  # GET /rounds/new
  def new
    @round = Round.new
  end

  # GET /rounds/1/edit
  def edit
  end

  # POST /rounds
  # POST /rounds.json
  def create
    @round = Round.new(round_params)

    respond_to do |format|
      if @round.save
        format.html { redirect_to @round, notice: 'Round was successfully created.' }
        format.json { render :show, status: :created, location: @round }
      else
        format.html { render :new }
        format.json { render json: @round.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /rounds/1
  # PATCH/PUT /rounds/1.json
  def update
    respond_to do |format|
      if @round.update(round_params)
        format.html { redirect_to @round, notice: 'Round was successfully updated.' }
        format.json { render :show, status: :ok, location: @round }
      else
        format.html { render :edit }
        format.json { render json: @round.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rounds/1
  # DELETE /rounds/1.json
  def destroy
    @round.destroy
    respond_to do |format|
      format.html { redirect_to rounds_url, notice: 'Round was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def standings
    response_json = standings_array

    response_json.map! do |row|
      if row['team']['id'] == current_team.try(&:id)
        row['result'] = Result.by(current_team).at(@round).latest.first.as_json
      end
      row
    end
    response_json.sort_by! do |row|
      ((row['result'] || {})['score'] || 0) * -1
    end

    render json: response_json
  end

  def scores
    span = params[:span].to_i
    span = 1 if span < 1
    times = []
    team_scores = {}
    team_scores[current_team] = [] if current_team
    top_n = params[:top].to_i
    top_n = 5 if top_n < 5
    top_n = 10 if top_n > 10 && !current_team.try(&:admin?)

    cachekey = "scores-adm#{current_team.try(&:admin?) ? 1 : 0}n-#{span}-#{top_n}-#{@round.id}"
    team_scores = Rails.cache.fetch(cachekey, expires_in: 1.minute) do
      # Top N team
      top_n_team_ids = standings_array.reject { |team|
        team['team']['id'] == current_team.try(&:id)
      }.first(top_n).map { |team| team['team']['id'] }

      now = Time.zone.now
      Result.timesliced_score_for_round(@round, span.minutes) { |round, team, t|
        t <= now &&
        (
          current_team.try(&:admin?) ||
          t <= (@round.ends_at - 1.hour)
        )
      }.map do |team, scores|
        next if team[:id] != current_team.try(:id) && team[:name] != 'x' && !top_n_team_ids.include?(team[:id])
        [team[:name], *scores]
      end.compact.to_json
    end

    render json: team_scores
  end

  def self_scores
    unless current_team
      return render json: []
    end

    span = params[:span].to_i
    span = 1 if span < 1

    cachekey = "selfscores-adm#{current_team.id}-#{span}-#{@round.id}"
    team_scores = Rails.cache.fetch(cachekey, expires_in: 1.minute) do
      now = Time.zone.now
      Result.timesliced_score_for_round(@round, span.minutes, teams: [current_team.id]) { |round, team, t|
        t <= now
      }.map do |team, scores|
        [team[:name], *scores]
      end.compact.to_json
    end

    render json: team_scores
  end

  def edit_report
    unless @round.require_report?
      return render status: 404, text: "(\xE0\xB9\x91\xE2\x95\xB9\xE2\x97\xA1\xE2\x95\xB9\xE0\xB9\x91)"
    end

    @report = current_team.final_reports.for(@round).first || FinalReport.new
  end

  def submit_report
    unless @round.require_report?
      return render status: 404, text: "(\xE0\xB9\x91\xE2\x95\xB9\xE2\x97\xA1\xE2\x95\xB9\xE0\xB9\x91)"
    end

    @report = current_team.final_reports.for(@round).first || FinalReport.new
    if @report.new_record?
      @report.team = current_team
      @report.round = @round
    end
    @report.update(params.permit(:ami_id, :benchmark_option, :language, :note))
    @saved_report = @report.valid?

    render :edit_report
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_round
    @round = Round.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def round_params
    params.require(:round).permit(:title, :description, :starts_at, :ends_at, :public_description, :public, :require_report)
  end

  def require_participant
    unless current_team && (current_team.admin? || current_team.participate?(@round))
      redirect_to login_path
    end
  end

  def require_viewer
    unless @round.public? || (current_team && (current_team.admin? || current_team.participate?(@round)))
      redirect_to login_path
    end
  end

  def standings_array
    Rails.cache.fetch("round-#{current_team.try(&:admin?) ? 1 : 0}-standings-#{@round.id}", expires_in: 10.minutes) do
      time = Time.zone.now
      time = @round.ends_at - 1.hour if !current_team.try(&:admin?) && time > (@round.ends_at - 1.hour)

      @round.teams_with_latest_result(time).map { |team_with_latest|
        team = team_with_latest[0]
        result = team_with_latest[1]
        { team: team, result: result }
      }.sort_by { |obj|
        (obj[:result].try(:score) || 0) * -1
      }.as_json
    end
  end
end
