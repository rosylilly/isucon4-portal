class DashboardController < ApplicationController
  before_action :basic_auth, only: %i(final_reports)

  def index
  end

  def final_reports
    reports = []

    FinalReport.all.includes(:team, :round).joins(:team, :round).each do |fr|
      bo = fr.benchmark_option
      bo.sub!(/\A((\.\/)?benchmarker)?( *bench)?/, '')
      bo = "--workload 1" if bo == "1"

      result = Result.by(fr.team).at(fr.round).latest.first
      sl = result.try(:submission_log)

      r = {
        id: fr.id,
        round_id: fr.round_id,
        round: fr.round.title,
        team_id: fr.team_id,
        team: fr.team.name,
        ami_id: fr.ami_id,
        language: fr.language,
        note: fr.note,
        benchmark_option: bo,
        benchmark_option_original: fr.benchmark_option
      }
      r[:result] =  {
        score: result.score,
        successes: result.successes,
        fails: result.fails
      } if result
      r[:meta] = JSON.parse(sl.meta) if sl

      reports << r
    end

    render json: reports
  end

  private

  def basic_auth
    authenticate_or_request_with_http_basic do |user, pass|
      user == 'isucon' && pass == 'on-memory'
    end
  end
end
