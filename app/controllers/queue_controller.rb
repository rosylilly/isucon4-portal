class QueueController < ApplicationController
  QUEUE_STATUS_KEY =  "isu4-portal:queue-status"
  QUEUE_STATUS_KEY_ADMIN =  "isu4-portal:queue-status-admin"
  before_action :require_admin, only: [:update]
  skip_before_action :verify_authenticity_token

  def index
  end

  def update
    queue = params[:queue]
    queue['updated_at'] = Time.now
    queue['pending'] ||= []
    queue['running'] ||= []

    [queue['pending'], queue['running']].each do |list|
      list.map do |w|
        team = Team.where(id: w['team_id']).first
        if team
          w['team'] = {
            id: team.id,
            name: team.name
          }
        end
      end
    end

    Redis.current.set QUEUE_STATUS_KEY_ADMIN, queue.to_json

    [queue['pending'], queue['running']].each do |list|
      list.map do |job|
        job.delete 'options'
      end
    end

    Redis.current.set QUEUE_STATUS_KEY, queue.to_json

    render status: :no_content, text: ''
  end

  def status
    status =  JSON.parse(Redis.current.get(current_team.try(:admin?) ? QUEUE_STATUS_KEY_ADMIN : QUEUE_STATUS_KEY) \
           || '{"running": [], "pending": []}')
    render json: status
  end
end
