class SessionController < ApplicationController
  def create
    api_key = ApiKey.where(secret: params[:api_key]).first
    unless api_key
      render text: "Wrong API key", status: 401
      return
    end

    session[:api_key] = params[:api_key]

    if session[:back_to]
      redirect_to session[:back_to]
      return
    end

    redirect_to root_path
  end

  def destroy
    session[:api_key] = nil

    redirect_to login_path
  end

  def new
  end
end
