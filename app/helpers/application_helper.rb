module ApplicationHelper
  def self.redcarpet
    @redcarpet ||= Redcarpet::Markdown.new(Redcarpet::Render::HTML)
  end

  def markdown(text)
    ApplicationHelper.redcarpet.render(text.to_s).html_safe
  end
end
