# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141107082731) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "api_keys", force: true do |t|
    t.string   "secret"
    t.integer  "team_id"
    t.integer  "round_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "final_reports", force: true do |t|
    t.integer  "round_id",         null: false
    t.integer  "team_id",          null: false
    t.string   "ami_id"
    t.string   "benchmark_option"
    t.string   "language"
    t.text     "note"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "final_reports", ["round_id"], name: "index_final_reports_on_round_id", using: :btree
  add_index "final_reports", ["team_id"], name: "index_final_reports_on_team_id", using: :btree

  create_table "qualifications", force: true do |t|
    t.integer  "team_id"
    t.integer  "round_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "results", force: true do |t|
    t.integer  "team_id"
    t.integer  "round_id"
    t.integer  "score"
    t.integer  "successes"
    t.integer  "fails"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "results", ["round_id"], name: "index_results_on_round_id", using: :btree
  add_index "results", ["team_id"], name: "index_results_on_team_id", using: :btree

  create_table "rounds", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.datetime "starts_at"
    t.datetime "ends_at"
    t.datetime "report_deadline_at"
    t.boolean  "public"
    t.boolean  "require_report"
    t.text     "public_description"
  end

  create_table "scores", force: true do |t|
    t.integer  "team_id"
    t.string   "round_id"
    t.integer  "score"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "submission_logs", force: true do |t|
    t.string   "ip_address"
    t.integer  "api_key_id"
    t.integer  "team_id"
    t.integer  "result_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "message"
    t.text     "meta"
  end

  create_table "teams", force: true do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean  "admin"
  end

end
