class AddReportDeadlineAtToRounds < ActiveRecord::Migration
  def change
    add_column :rounds, :report_deadline_at, :datetime
  end
end
