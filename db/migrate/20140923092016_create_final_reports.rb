class CreateFinalReports < ActiveRecord::Migration
  def change
    create_table :final_reports do |t|
      t.belongs_to :round, index: true, null: false
      t.belongs_to :team, index: true, null: false
      t.string :ami_id
      t.string :benchmark_option
      t.string :language
      t.text :note

      t.timestamps null: false
    end
  end
end
