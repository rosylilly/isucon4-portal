class AddRequireReportToRounds < ActiveRecord::Migration
  def change
    add_column :rounds, :require_report, :boolean
  end
end
