class CreateQualifications < ActiveRecord::Migration
  def change
    create_table :qualifications do |t|
      t.integer :team_id
      t.integer :round_id

      t.timestamps null: false
    end
  end
end
