class AddAdminToTeams < ActiveRecord::Migration
  def change
    add_column :teams, :admin, :boolean
  end
end
