class AddPublicToRounds < ActiveRecord::Migration
  def change
    add_column :rounds, :public, :boolean
  end
end
