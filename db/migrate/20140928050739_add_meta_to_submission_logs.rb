class AddMetaToSubmissionLogs < ActiveRecord::Migration
  def change
    add_column :submission_logs, :meta, :text
  end
end
