class RemoveUnusedColumns < ActiveRecord::Migration
  def change
    remove_column :teams, :leader_id
    remove_column :teams, :qualifier_day
    remove_column :teams, :passed_final
  end
end
