class CreateTeams < ActiveRecord::Migration
  def change
    create_table :teams do |t|
      t.string :name
      t.integer :leader_id
      t.integer :qualifier_day
      t.boolean :passed_final

      t.timestamps null: false
    end
  end
end
