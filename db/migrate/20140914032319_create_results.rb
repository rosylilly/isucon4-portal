class CreateResults < ActiveRecord::Migration
  def change
    create_table :results do |t|
      t.belongs_to :team, index: true
      t.belongs_to :round, index: true
      t.integer :score
      t.integer :successes
      t.integer :fails

      t.timestamps null: false
    end
  end
end
