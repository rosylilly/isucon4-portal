class AddMessageToSubmissionLogs < ActiveRecord::Migration
  def change
    add_column :submission_logs, :message, :string
  end
end
