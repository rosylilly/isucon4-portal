class AddPublicDescriptionToRounds < ActiveRecord::Migration
  def change
    add_column :rounds, :public_description, :text
  end
end
