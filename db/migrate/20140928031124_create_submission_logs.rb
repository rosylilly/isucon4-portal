class CreateSubmissionLogs < ActiveRecord::Migration
  def change
    create_table :submission_logs do |t|
      t.string :ip_address
      t.integer :api_key_id
      t.integer :team_id
      t.integer :result_id

      t.timestamps null: false
    end
  end
end
