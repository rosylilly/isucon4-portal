class CreateRounds < ActiveRecord::Migration
  def change
    create_table :rounds do |t|
      t.string :title
      t.text :description

      t.timestamps null: false
    end
  end
end
