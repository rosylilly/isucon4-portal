# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

require 'google_spreadsheet'

allTeamListUrl = 'https://docs.google.com/spreadsheets/d/1UCahBa455LLfEApJXy_5uJe6MT2yqgErhr2h6_TEZXI/edit'

if Rails.env.development?
  allTeamListUrl = 'https://docs.google.com/a/cookpad.jp/spreadsheets/d/18IsvWLbOQrWaI_YVVmdVJYaZGK8q5-4EBdA_X_hFX74/edit'
end

print "Google Account(User Name) > "
googleUser = STDIN.gets.strip
print "Google Account(Password)  > "
googlePass = STDIN.gets.strip

begin
  googleSession = GoogleSpreadsheet.login(googleUser, googlePass)
rescue GoogleDrive::AuthenticationError
  puts "Google 認証に失敗しました。ユーザー名とパスワードを確認の上、再度操作を行ってください。"
  exit 1
end

allTeamSheet = googleSession.spreadsheet_by_url(allTeamListUrl)

roundParams = {
  "9月27日(土)" => {
    starts_at: Time.parse('2014-9-27 10:00:00 +09:00'),
    ends_at: Time.parse('2014-9-27 18:00:00 +09:00')
  },
  "9月28日(日)" => {
    starts_at: Time.parse('2014-9-28 10:00:00 +09:00'),
    ends_at: Time.parse('2014-9-28 18:00:00 +09:00')
  }
}

ws = allTeamSheet.worksheets[0]
ws.rows.each_with_index do |row, index|
  teamName = row[3].strip
  roundDay = row[8].strip

  next if index == 0 || teamName.blank? || roundDay.blank?

  roundTitle = "オンライン予選 #{roundDay}"
  round = Round.where(title: roundTitle).first || Round.create!(
    roundParams[roundDay].merge(
      title: roundTitle
    )
  )
  team = Team.where(name: teamName).first || Team.create!(name: teamName)
  unless round.teams.include?(team)
    round.teams << team
    round.save
  end
  apiKey = team.api_keys.where(round_id: round.id).first || ApiKey.create!(
    team: team,
    round: round
  )
  ws[index+1, 10] = apiKey.secret
  puts "#{team.name}: #{apiKey.secret}"
end
ws.save
